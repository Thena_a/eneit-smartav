#propocion de credencial  5.4 x 8.5 aprox  
################ Modulos ####################### 
import cv2 
from PIL import Image
import os
import argparse 
import pytesseract as ocr
################################################

########## direccion de la imagen ############## 
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,help="comando para el archivo de la imagen")
args = vars(ap.parse_args())

########## Proceso de imagen ################### 
imgID = cv2.imread(args['image'])
x,y,channel = imgID.shape


y1Face = int(y * (0.5/8.5))
y2Face = int(y * (2.5/8.5))
x1Face = int(x * (1.8/5.4))
x2Face = int(x * (4.6 / 5.4))

y1Name = int(y * 2.6 / 8.5)
y2Name = int(y * 4.7 / 8.5)
x1Name = int(x * 1.7 / 5.4)
x2Name = int(x * 2.5 / 5.4)

y1Dir = int(y * 2.6 / 8.5)
y2Dir = int(y * 6 / 8.5)

x1Dir = int(x * 2.7 / 5.4)
x2Dir = int(x * 3.6 / 5.4)


fotos =[]

fotos.append(imgID[x1Face:x2Face, y1Face:y2Face])
fotos.append(imgID[x1Name:x2Name , y1Name:y2Name])
fotos.append(imgID[x1Dir:x2Dir, y1Dir:y2Dir])

for foto in range(1,len(fotos)):
    fotos[foto] = cv2.cvtColor(fotos[foto],cv2.COLOR_BGR2GRAY)
    fotos[foto] = cv2.threshold(fotos[foto], 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

fileName = "name.png"
fileDic = "Dic.png"

cv2.imwrite(fileName, fotos[1])
cv2.imwrite(fileDic, fotos[2])


txtName = ocr.image_to_string(Image.open(fileName))
txtDic = ocr.image_to_string(Image.open(fileDic))

os.remove(fileName)
os.remove(fileDic)

os.system("cls")

print("Texto encontroado  como nombre: \n")
print(txtName)

print("Texto encotnrado como direccion:\n")
print(txtDic)


cv2.imshow("todo", imgID)
cv2.imshow("face" , fotos[0])
cv2.imshow("name" , fotos[1])
cv2.imshow("Dic", fotos[2])
cv2.waitKey(0)

