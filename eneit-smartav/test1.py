from PIL import Image 
import pytesseract as ocr 
import cv2
import os  
import argparse


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Direccion de la imagen")
ap.add_argument("-p", "--preprocess", type=str, default="thresh", help="type of preprocessing to be done")
args = vars(ap.parse_args())

img = cv2.imread(args["image"],0)
gray = cv2.threshold(img , 0,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]


nombreimg = gray[ 145:265 , 262:493]
direcimg = gray[255:375 , 262:600]

filename = "nombre.png"
filename2 = "direccion.png"
#265 145, 493 262

cv2.imwrite(filename, nombreimg)
cv2.imwrite(filename2, direcimg)

nombre = ocr.image_to_string(Image.open(filename))
direccion = ocr.image_to_string(Image.open(filename2))


os.remove(filename)
os.remove(filename2)
print("nombre encontrado:")
print(nombre)
print("")
print("direccion encontrada:")
print(direccion)

cv2.imshow("gris",gray)
cv2.imshow("nombre", nombreimg)
cv2.imshow("dic", direcimg)
cv2.waitKey(0)


