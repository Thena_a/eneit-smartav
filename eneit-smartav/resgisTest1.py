from __future__ import print_function as printf
import cv2
import numpy as np
import os
from PIL import Image 
import pytesseract as ocr


def alignImages(im1, im2, MAX_FEATURES, GOOD_MATCH_PERCENT):

 
  # Convert images to grayscale
  im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
  im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
   
  # Detect ORB features and compute descriptors.
  orb = cv2.ORB_create(MAX_FEATURES)
  keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
  keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)
   
  # Match features.
  matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
  matches = matcher.match(descriptors1, descriptors2, None)
   
  # Sort matches by score
  matches.sort(key=lambda x: x.distance, reverse=False)
 
  # Remove not so good matches
  numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
  matches = matches[:numGoodMatches]
 
  # Draw top matches
  imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
  cv2.imwrite("matches.jpg", imMatches)
   
  # Extract location of good matches
  points1 = np.zeros((len(matches), 2), dtype=np.float32)
  points2 = np.zeros((len(matches), 2), dtype=np.float32)
 
  for i, match in enumerate(matches):
    points1[i, :] = keypoints1[match.queryIdx].pt
    points2[i, :] = keypoints2[match.trainIdx].pt
   
  # Find homography
  h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
 
  # Use homography
  height, width, channels = im2.shape
  im1Reg = cv2.warpPerspective(im1, h, (width, height))
   
  return im1Reg, h


imgR = cv2.imread('tena_ine2.png',cv2.IMREAD_COLOR) 
imgA = cv2.imread('octavio_ine2.png',cv2.IMREAD_COLOR)

imgID , h  = alignImages(imgA , imgR, 10000, 0.01 )

x,y,channel = imgID.shape


y1Face = int(y * (0.5/8.5))
y2Face = int(y * (2.5/8.5))
x1Face = int(x * (1.8/5.4))
x2Face = int(x * (4.6 / 5.4))

y1Name = int(y * 2.6 / 8.5)
y2Name = int(y * 4.7 / 8.5)
x1Name = int(x * 1.7 / 5.4)
x2Name = int(x * 2.5 / 5.4)

y1Dir = int(y * 2.6 / 8.5)
y2Dir = int(y * 6 / 8.5)

x1Dir = int(x * 2.7 / 5.4)
x2Dir = int(x * 3.6 / 5.4)


fotos =[]

fotos.append(imgID[x1Face:x2Face, y1Face:y2Face])
fotos.append(imgID[x1Name:x2Name , y1Name:y2Name])
fotos.append(imgID[x1Dir:x2Dir, y1Dir:y2Dir])

for foto in range(1,len(fotos)):
    fotos[foto] = cv2.cvtColor(fotos[foto],cv2.COLOR_BGR2GRAY)
    fotos[foto] = cv2.threshold(fotos[foto], 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

fileName = "name.png"
fileDic = "Dic.png"

cv2.imwrite(fileName, fotos[1])
cv2.imwrite(fileDic, fotos[2])


txtName = ocr.image_to_string(Image.open(fileName))
txtDic = ocr.image_to_string(Image.open(fileDic))

os.remove(fileName)
os.remove(fileDic)

os.system("cls")

print("Texto encontroado  como nombre: \n")
print(txtName)

print("Texto encotnrado como direccion:\n")
print(txtDic)

img2 = cv2.imread('matches.jpg')

cv2.imshow("original" , imgA)
cv2.imshow("alineada" , imgID)
cv2.imshow("face" , fotos[0])
cv2.imshow("name" , fotos[1])
cv2.imshow("Dic", fotos[2])
cv2.imshow("matches", img2)
cv2.waitKey(0)