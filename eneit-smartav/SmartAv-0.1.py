from PIL import Image
import cv2,os
import numpy as np 
import pytesseract as ocr
import BBox
############globales#####################3
cap = cv2.VideoCapture(0) # camara principal
face_cascade = cv2.CascadeClassifier('face.xml')


######## Tomando foto de la persona ###########

while(True):
    r , img = cap.read()
    h, w, ch = img.shape 
    

    x1 = w/4
    y1 = h/6
    color =(0,0,255)
    BBoxSection = BBox.BBox(x1 , y1 , w/2, h*4/6, color)
    
 
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)


    if(len(faces) > 0):
        xx, yy, w ,h = faces[0]
        BBoxFace = BBox.BBox(xx,yy,w,h)  
    else:
        BBoxFace = BBox.BBox(0,0,0,0)


    if( BBoxFace.area > BBoxSection.area/2 and len(faces) and BBoxSection.pt1[0] < BBoxFace.pt1[0] and BBoxSection.pt1[1] < BBoxFace.pt1[1] and BBoxSection.pt2[0] > BBoxFace.pt2[0] and BBoxSection.pt2[1] > BBoxFace.pt2[1]  ):
        BBoxSection.color=(0,255,0)
        imgFace = img
        break



    BBoxSection.draw(img)
    BBoxFace.draw(img)
    cv2.imshow('Face read' , img)
    cv2.waitKey(1) 
    
############################################################################       
###################  tomando imagen de la credencial   #####################
while(True):



############################################################################


cap.release()
cv2.destroyAllWindows()


#while(True):
#     if(  cv2.waitKey(1) & 0xFF == ord('q')):
#        break