from PIL import Image
import cv2,os
import numpy as np 
import pytesseract as ocr
import BBox
import imutils
from random import randint as rnd
cap = cv2.VideoCapture(0)

while(True):
    image = cap.read()[1]

    ratio = image.shape[0] / 500.0
    orig = image.copy()
    image = imutils.resize(image, height = 500)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(gray, 1, 200)

    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:5]
    
    # loop over the contours
    for c in cnts:
      # approximate the contour
      peri = cv2.arcLength(c, True)
      approx = cv2.approxPolyDP(c, 0.02 * peri, False)
    
      # if our approximated contour has four points, then we
      # can assume that we have found our screen
      if (3<len(approx)<100) :
        screenCnt = approx
        break
    
    # show the contour (outline) of the piece of paper
    
    cv2.drawContours(image, [screenCnt], -1, (0, 255, 0), 2)
    cv2.imshow("Outline", image)
    cv2.imshow("ed", edged)
    
    if(  cv2.waitKey(1) & 0xFF == ord('q')):
        break
cv2.waitKey(0)
cv2.destroyAllWindows()