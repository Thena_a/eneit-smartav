try:
    import cv2
except ImportError:
    print("OpenCV no esta instalado")

class BBox:
    def __init__(self, x,y,w,h,color = (0,255,0) , tk = 2): # color = BGR
       self.pt1 = int(x),int(y)
       self.pt2 = int(x+w) , int(y+h)
       self.area = w*h
       self.color = color
       self.tk = tk
    def draw(self,img):
        cv2.rectangle(img,self.pt1, self.pt2, self.color, self.tk)

